import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Settings } from '../../app/settings';

@Injectable()
export class DatabaseProvider {

  constructor(private storage: Storage) {
    // this.storage.clear();
    this.setupDatabase();
  }

  async setupDatabase(): Promise<any> {
    if (!await this.storage.get('routes')) {
      await this.saveRoutes([]);
    }
    if (!await this.storage.get('settings')) {
      await this.saveSettings(Settings.default);
    }
  }

  async getRoute(index: number): Promise<any> {
    return (await this.getRoutes())[index];
  }

  async getRoutes(): Promise<IRoute[]> {
    return await this.storage.get('routes');
  }

  async saveRoute(route: IRoute): Promise<any> {
    let routes: IRoute[] = await this.getRoutes();
    routes.push(route);

    return await this.saveRoutes(routes);
  }

  async saveRoutes(routes: IRoute[]): Promise<any> {
    return await this.storage.set('routes', routes);
  }

  async deleteRoute(index: number): Promise<any> {
    let routes: IRoute[] = await this.getRoutes();
    routes.splice(index, 1);

    return await this.saveRoutes(routes);
  }

  async getSettings(): Promise<any> {
    return await this.storage.get('settings');
  }

  async saveSettings(settings: any): Promise<any> {
    return await this.storage.set('settings', settings);
  }

}
