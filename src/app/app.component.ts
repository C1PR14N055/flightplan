import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { DatabaseProvider } from '../providers/database/database';
import { Settings } from './settings';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;

  constructor(private platform: Platform, private statusBar: StatusBar,
    private splashScreen: SplashScreen, private database: DatabaseProvider) {
    this.platform.ready().then(() => {
      this.loadSettings();
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
    });
  }

  async loadSettings(): Promise<any> {
    Settings.current = await this.database.getSettings();
  }
}
