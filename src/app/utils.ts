import { Settings } from '../app/settings';

export class Utils {

  static DEBUG_ENABLED: boolean = true;

  public static log(...data: Array<any>): void {
    if (this.DEBUG_ENABLED) {
      // tslint:disable-next-line:no-console
      Utils.log = console.log.bind(window.console);
      Utils.log(...data);
    }
  }

  public static getUTCTimeNow(): number {
    return new Date().getTime();
  }

  public static distance(coords: ILatLng[]): number {
    if (coords.length < 2) {
      return 0;
    }
    let distance: number = 0;
    for (let i: number = 1; i < coords.length; i++) {
      distance +=
        Math.acos(
          Math.sin(Math.PI * coords[i - 1].lat / 180)
          * Math.sin(Math.PI * coords[i].lat / 180)
          + Math.cos(Math.PI * coords[i - 1].lat / 180)
          * Math.cos(Math.PI * coords[i].lat / 180)
          * Math.cos(Math.PI * (coords[i - 1].lng - coords[i].lng) / 180)
        ) * 6370693.485653058;
    }

    return distance;
  }

  public static time(coords: ILatLng[]): number {
    return Utils.distance(coords) / (Settings.current.SPEED_KMPH / 3.6);
  }

  public static speed(distance: number, time: number, timestamp: number): number {
    return distance / ((time - (timestamp || new Date().getTime())) / 1000);
  }
}
