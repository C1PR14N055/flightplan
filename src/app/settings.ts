import * as CONST from '../app/constants';

export class Settings {
  private static _default: ISettings = {
    // Mathematical Values
    SPEED_KMPH: 120,
    GATE_RADIUS_METERS: 200,
    PROCEDURE_TIME_SECONDS: 60,
    // UI
    AIRCRAFT_ICON: 1,
    AIRCRAFT_ICON_COLOR: CONST.COLOR_ORANGE,
    MARKER_COLOR: CONST.COLOR_MAGENTA,
    CIRCLE_COLOR: CONST.COLOR_MAGENTA,
    POLYLINE_COLOR: CONST.COLOR_MAGENTA
  };

  private static _current: ISettings = Settings._default;

  public static get default(): ISettings {
    return this._default;
  }

  public static get current(): ISettings {
    return this._current;
  }

  public static set current(settings: ISettings) {
    this._current = settings;
  }

}
