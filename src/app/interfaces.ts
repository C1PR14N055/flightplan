interface ILatLng {
  lat: number;
  lng: number;
}

interface IGate {
  nr: number;
  marker: google.maps.Marker;
  circle: google.maps.Circle;
  eta: number;
  proc?: boolean;
}

interface IFlightPlan {
  gates: IGate[];
  polylines: google.maps.Polyline[];
}

interface IInstruments {
  eta: number;
  etac?: number; // eta correction
  dst: number;
  spd: number;
  spdc?: number; // speed correction
  hdg: number;
  hdgc?: number; // heading correction
  alt: number;
}

interface IRoute {
  name: string;
  date: number;
  legs: ILatLng[][];
}

interface ISettings {
  // Mathematical Values
  SPEED_KMPH: number;
  GATE_RADIUS_METERS: number;
  PROCEDURE_TIME_SECONDS: number;
  // UI
  AIRCRAFT_ICON: number;
  AIRCRAFT_ICON_COLOR: string;
  MARKER_COLOR: string;
  CIRCLE_COLOR: string;
  POLYLINE_COLOR: string;
}
