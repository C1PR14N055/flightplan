export const COLOR_RED: string = '#F44336';
export const COLOR_MAGENTA: string = '#E91E63';
export const COLOR_PURPLE: string = '#9C27B0';
export const COLOR_BLUE: string = '#2196F3';
export const COLOR_GREEN: string = '#4CAF50';
export const COLOR_YELLOW: string = '#FFEB3B';
export const COLOR_ORANGE: string = '#FF9800';

// tslint:disable-next-line:max-line-length
export const AIRCRAFT_SVG: string = 'M362.985,430.724l-10.248,51.234l62.332,57.969l-3.293,26.145 l-71.345-23.599l-2.001,13.069l-2.057-13.529l-71.278,22.928l-5.762-23.984l64.097-59.271l-8.913-51.359l0.858-114.43 l-21.945-11.338l-189.358,88.76l-1.18-32.262l213.344-180.08l0.875-107.436l7.973-32.005l7.642-12.054l7.377-3.958l9.238,3.65 l6.367,14.925l7.369,30.363v106.375l211.592,182.082l-1.496,32.247l-188.479-90.61l-21.616,10.087l-0.094,115.684';
// tslint:disable-next-line:max-line-length
// export const AIRCRAFT_SVG: string = 'M288,32l-16,128l-16,48v24L108,348.8v-39.5c0-11.7-2.6-21.3-6-21.3s-6,9.7-6,21.3V424h12v-16h164v48l-80,66.7v34.7l10.7,10.7H272v-8h16v-48h2.7l8,64h42.7l8-64h2.7v48h16v8h69.3l10.7-10.7v-34.7L368,456v-48h164v16h12V309.3c0-11.7-2.6-21.3-6-21.3s-6,9.7-6,21.3v39.5L384,232v-24l-16-48L352,32c-21.3-96-26.6-96-32-96S309.3-64,288,32z';
export const GATE_MARKER_SVG: string = 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z';
// tslint:disable-next-line:max-line-length
export const GOOGLE_MAPS_URL: string =
  "http://maps.googleapis.com/maps/api/js?key=AIzaSyDYYITjk0rogGhdA5mLEHUGVsG7Dn-YU68&callback=onGoogleMapsLoaded";
// tslint:disable-next-line:max-line-length
export const GOOGLE_MAPS_URL_DEBUG: string = 'http://maps.googleapis.com/maps/api/js?callback=onGoogleMapsLoaded';
