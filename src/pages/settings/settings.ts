import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController, Events } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { Settings } from '../../app/settings';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  settings: ISettings = null;
  alert: any = null;
  toast: any = null;

  constructor(private database: DatabaseProvider, private toastCtrl: ToastController,
    private alertCtrl: AlertController, private events: Events) {
    this.settings = Settings.current;
  }

  private getSettings(): void {
    this.settings = Settings.current;
  }

  private async saveSettings(): Promise<any> {
    await this.database.saveSettings(this.settings);
    Settings.current = this.settings;
    this.events.publish('settings_updated');
    this.toast = this.toastCtrl.create(
      {
        message: 'Settings updated!',
        duration: 3000,
        showCloseButton: true
      }
    );
    this.toast.present();
  }

  promptSaveSettings(): void {
    this.alert = this.alertCtrl.create({
      title: 'Settings',
      message: 'By changing settings ETAs will be invalidated! Do you want to continue?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            this.getSettings();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.saveSettings();
          }
        }
      ]
    });
    this.alert.present();
  }

}
