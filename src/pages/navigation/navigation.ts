import { Component, ElementRef, ViewChild } from '@angular/core';
import {
  AlertController, LoadingController, ModalController,
  ToastController, Events, FabContainer
} from 'ionic-angular';
import { Utils } from '../../app/utils';
import * as CONST from '../../app/constants';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Insomnia } from '@ionic-native/insomnia';
import { EstimatesPage } from '../estimates/estimates';
import { DatabaseProvider } from '../../providers/database/database';
import { Settings } from '../../app/settings';

@Component({
  selector: 'page-navigation',
  templateUrl: 'navigation.html'
})
export class NavigationPage {

  // google maps values
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('fab') fab: FabContainer;
  map: google.maps.Map;
  aircraft: google.maps.Marker;
  currentMapType: number = 3;

  // layout objects
  loading: any = null;
  alert: any = null;
  toast: any = null;

  // logic values
  flightPlan: IFlightPlan = {
    gates: [],
    polylines: [],
  };
  nextGate: IGate;
  navigating: boolean = false;

  // instruments, gps data
  instruments: IInstruments = {
    eta: null,
    etac: null,
    dst: null,
    spd: null,
    spdc: null,
    hdg: null,
    hdgc: null,
    alt: null
  };

  // gps observable
  watch: any = null;

  // current time
  timeNow: number = Utils.getUTCTimeNow();

  // flight plan editable
  edititable: boolean = false;

  constructor(private geolocation: Geolocation, private alertCtrl: AlertController,
    private loadingCtrl: LoadingController, private insomnia: Insomnia,
    private modalCtrl: ModalController, private toastCtrl: ToastController,
    private database: DatabaseProvider, private evets: Events) {
  }

  loadMap(): void {
    this.loading = this.loadingCtrl.create({ content: 'Loading map...' });
    this.loading.present();
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 10,
      disableDefaultUI: true,
      scaleControl: true,
      mapTypeId: google.maps.MapTypeId.HYBRID,
      center: { lat: 46.333, lng: 21.509 }
    });
    this.loading.dismiss();

    this.geolocation.getCurrentPosition()
      .then((data: any) => {
        this.map.panTo({ lat: data.coords.latitude, lng: data.coords.longitude });
        this.aircraft = new google.maps.Marker({
          position: { lat: data.coords.latitude, lng: data.coords.longitude },
          map: this.map,
          icon: {
            path: CONST.AIRCRAFT_SVG,
            scale: 0.1,
            strokeOpacity: 1,
            strokeColor: Settings.current.AIRCRAFT_ICON_COLOR,
            fillColor: Settings.current.AIRCRAFT_ICON_COLOR,
            fillOpacity: 1,
            strokeWeight: 1,
            anchor: new google.maps.Point(340, 320)
          },
        });
      })
      .catch(() => {
        this.alert = this.alertCtrl.create({
          title: 'Oops',
          message: 'Flightplan can\'t locate you! Enable GPS, grant permission and try again!',
          buttons: ['Ok']
        });
        this.alert.present();
      });
  }

  switchMapType(): void {
    this.currentMapType = ++this.currentMapType > 4 ? this.currentMapType = 1 : this.currentMapType;
    switch (this.currentMapType) {
      case 1: {
        this.map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
        break;
      }
      case 2: {
        this.map.setMapTypeId(google.maps.MapTypeId.TERRAIN);
        break;
      }
      case 3: {
        this.map.setMapTypeId(google.maps.MapTypeId.HYBRID);
        break;
      }
      case 4: {
        this.map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
        break;
      }
    }
  }

  addGate(position?: any): void {
    let gate: IGate = null;
    let marker: google.maps.Marker = null;
    let circle: google.maps.Circle = null;

    marker = new google.maps.Marker({
      position: position || this.map.getCenter(),
      draggable: this.edititable,
      label: this.flightPlan.gates.length.toString(),
      icon: {
        path: CONST.GATE_MARKER_SVG,
        labelOrigin: new google.maps.Point(0, -27.5),
        fillOpacity: 1,
        fillColor: CONST.COLOR_MAGENTA
      },
      map: this.map
    });

    marker.addListener('drag', () => {
      this.moveCircle(gate, marker.getPosition());
      this.movePolylines(gate, marker.getPosition());
    });
    marker.addListener('dragend', () => {
      this.moveCircle(gate, marker.getPosition());
      this.movePolylines(gate, marker.getPosition());
    });

    circle = new google.maps.Circle({
      strokeColor: CONST.COLOR_MAGENTA,
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: CONST.COLOR_MAGENTA,
      fillOpacity: 0.35,
      center: {
        lat: marker.getPosition().lat(),
        lng: marker.getPosition().lng()
      },
      radius: Settings.current.GATE_RADIUS_METERS,
      map: this.map
    });

    gate = {
      nr: this.flightPlan.gates.length,
      marker: marker,
      circle: circle,
      eta: null,
    };
    this.flightPlan.gates.push(gate);

    this.addPolyline();
  }

  deleteGate(): void {
    let lastGateNr: number = this.flightPlan.gates.length - 1;
    if (lastGateNr > -1) {
      this.flightPlan.gates[lastGateNr].marker.setMap(null);
      this.flightPlan.gates[lastGateNr].circle.setMap(null);
      this.flightPlan.gates.splice(-1, 1);
    }
    this.deletePolyline();
  }

  addPolyline(): void {
    if (this.flightPlan.gates.length > 1) {
      let path: ILatLng[] = [
        {
          lat: this.flightPlan.gates[this.flightPlan.gates.length - 2].marker.getPosition().lat(),
          lng: this.flightPlan.gates[this.flightPlan.gates.length - 2].marker.getPosition().lng()
        },
        {
          lat: this.flightPlan.gates[this.flightPlan.gates.length - 1].marker.getPosition().lat(),
          lng: this.flightPlan.gates[this.flightPlan.gates.length - 1].marker.getPosition().lng()
        }
      ];
      let polyline: google.maps.Polyline = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: CONST.COLOR_MAGENTA,
        strokeOpacity: 1,
        strokeWeight: 2,
        editable: this.edititable,
        map: this.map
      });

      this.flightPlan.polylines.push(polyline);

      // google.maps.event.addListener(polyline.getPath(),
      //   'insert_at', () => {
      //     Utils.log('insert_at', polyline.getPath().getAt(0));
      //   });

      // google.maps.event.addListener(polyline.getPath(),
      //   'dragend', () => {
      //     Utils.log('dragend', polyline.getPath().getAt(0));
      //   });

      // google.maps.event.addListener(polyline,
      //   'click', () => {
      //     Utils.log('click', polyline.getPath().getAt(0));
      //   });

      // google.maps.event.addListener(polyline.getPath(),
      //   'dragstart', (index: number) => {
      //     event.stopPropagation();
      //   });

      // google.maps.event.addListener(polyline.getPath(),
      //   'set_at', (index: number) => {
      // if (!this.editingLine) {
      //   this.editingLine = true;
      //   Utils.log('set_at index', index);
      //   let gate: IGate = null;
      //   if (index == 0) {
      //     gate = this.flightPlan.gates[this.flightPlan.gates.length - 2];
      //     Utils.log('modif to gate ', gate.nr);
      //     this.movePolylines(gate, gate.marker.getPosition());
      //     // this.moveMarker(gate, polyline.getPath().getAt(0));
      //     // this.moveCircle(gate, polyline.getPath().getAt(0));
      //   } else if (index == polyline.getPath().getLength() - 1) {
      //     gate = this.flightPlan.gates[this.flightPlan.gates.length - 1];
      //     Utils.log('modif to gate ', gate.nr);
      //     // this.moveMarker(gate, polyline.getPath().getAt(polyline.getPath().getLength() - 1));
      //     // this.moveCircle(gate, polyline.getPath().getAt(polyline.getPath().getLength() - 1));
      //     this.movePolylines(gate, gate.marker.getPosition());
      //   }
      //   this.editingLine = false;
      // }
      // });

      // Utils.log('path', this.flightPlan.polyline.getPath().getArray().toString());

    }
  }

  deletePolyline(): void {
    let lastPolylineNr: number = this.flightPlan.polylines.length - 1;
    if (lastPolylineNr > -1) {
      this.flightPlan.polylines[lastPolylineNr].setMap(null);
      this.flightPlan.polylines.splice(-1, 1);
    }
  }

  moveMarker(gate: IGate, position: google.maps.LatLng): void {
    gate.marker.setPosition(position);
  }

  moveCircle(gate: IGate, position: google.maps.LatLng): void {
    gate.circle.setCenter(position);
  }

  movePolylines(gate: IGate, position: google.maps.LatLng): void {
    let prevPoly: google.maps.Polyline = gate.nr > 0
      ? this.flightPlan.polylines[gate.nr - 1]
      : null;
    let nextPoly: google.maps.Polyline = this.flightPlan.polylines.length - 1 >= gate.nr
      ? this.flightPlan.polylines[gate.nr]
      : null;

    if (prevPoly) {
      prevPoly.getPath().setAt(
        prevPoly.getPath().getLength() - 1,
        position
      );
    }

    if (nextPoly) {
      nextPoly.getPath().setAt(
        0,
        position
      );
    }
  }

  navigate(): void {
    if (this.flightPlan.gates.length < 2) {
      this.toast = this.toastCtrl.create(
        {
          message: 'Add at least two waypoints first!',
          duration: 3000,
          showCloseButton: true
        }
      );
      this.toast.present();

      return;
    }
    if (!this.flightPlan.gates[0].eta) {
      this.toast = this.toastCtrl.create(
        {
          message: 'Set your estimates first!',
          duration: 3000,
          showCloseButton: true
        }
      );
      this.toast.present();

      return;
    }
    if (!this.navigating) {
      this.nextGate = this.flightPlan.gates[0];
      this.navigating = true;
      this.insomnia.keepAwake();
      this.setRouteEditable(false);
      this.fab.close();
      this.watch = this.geolocation.watchPosition({
        enableHighAccuracy: true,
        timeout: 3000,
        maximumAge: 0
      }).subscribe((gps: any) => {
        this.setInstrumentsData(gps);
      });
    } else {
      this.watch.unsubscribe();
      this.navigating = false;
      this.insomnia.allowSleepAgain();
    }
  }

  setInstrumentsData(gps: Geoposition): void {
    if (gps && gps.coords) {
      if (gps.coords.heading) {
        this.instruments.hdg = gps.coords.heading;
      }
      if (gps.coords.altitude) {
        this.instruments.alt = gps.coords.altitude;
      }
      if (gps.coords.latitude && gps.coords.longitude) {
        this.aircraft.setPosition({ lat: gps.coords.latitude, lng: gps.coords.longitude });

        let icon: any = this.aircraft.getIcon();
        icon.rotation = gps.coords.heading;
        this.aircraft.setIcon(icon);
        this.aircraft.setPosition({ lat: gps.coords.latitude, lng: gps.coords.longitude });
        this.map.panTo({ lat: gps.coords.latitude, lng: gps.coords.longitude });

        this.hasReachedNextGate({ lat: gps.coords.latitude, lng: gps.coords.longitude });

        // Distance
        if (this.nextGate.nr > 0) {
          let coords: ILatLng[] = [];
          for (let i: number = 0; i < this.flightPlan.polylines[this.nextGate.nr - 1].getPath().getLength(); i++) {
            let point: ILatLng = { lat: 0, lng: 0 };
            point.lat = this.flightPlan.polylines[this.nextGate.nr - 1].getPath().getAt(i).lat();
            point.lng = this.flightPlan.polylines[this.nextGate.nr - 1].getPath().getAt(i).lng();
            coords.push(point);
          }
          this.instruments.dst = Utils.distance(coords) / 1000;
        }
        // Distance

        if (gps.coords.speed) {
          this.instruments.spd = gps.coords.speed * 3.6;
          this.instruments.dst = Utils.distance([
            { lat: gps.coords.latitude, lng: gps.coords.longitude },
            { lat: this.nextGate.marker.getPosition().lat(), lng: this.nextGate.marker.getPosition().lng() }
          ]);
          this.instruments.spdc = Utils.speed(
            this.instruments.dst, this.nextGate.eta, gps.timestamp
          ) - gps.coords.speed;
        }
      }
    }
  }

  setEstimates(): void {
    if (this.flightPlan.gates.length > 1) {
      let modal: any = this.modalCtrl.create(EstimatesPage, { flightPlan: this.flightPlan });
      modal.onDidDismiss((flightPlan: IFlightPlan) => {
        this.flightPlan = flightPlan;
      });
      modal.present();
    } else {
      this.toast = this.toastCtrl.create(
        {
          message: 'Add at least two waypoints first!',
          duration: 3000,
          showCloseButton: true
        }
      );
      this.toast.present();
    }
  }

  hasReachedNextGate(position: ILatLng): void {
    for (let i: number = 0; i < this.flightPlan.gates.length; i++) {
      let g: IGate = this.flightPlan.gates[i];
      if (Utils.distance([position, { lat: g.marker.getPosition().lat(), lng: g.marker.getPosition().lng() }])
        <= Settings.current.GATE_RADIUS_METERS
        && g.nr == this.nextGate.nr) {
        if (i + 1 < this.flightPlan.gates.length) {
          this.nextGate = this.flightPlan.gates[i + 1];
        } else {
          this.alert = this.alertCtrl.create(
            {
              title: 'Yayy!',
              message: 'You finished the planned flight! Now nail the landing!',
              buttons: [
                {
                  text: 'Ok'
                }
              ]
            });
          this.alert.present();
          this.navigate();
        }
      }
    }
  }

  setRouteEditable(editable: boolean): void {
    this.edititable = editable;
    for (let g of this.flightPlan.gates) {
      g.marker.setDraggable(editable);
    }
    for (let p of this.flightPlan.polylines) {
      p.setEditable(editable);
    }
  }

  promptDeleteRoute(): void {
    this.alert = this.alertCtrl.create({
      title: 'Delete',
      message: 'Do you really want to delete the current route from the map?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.deleteRoute();
          }
        }
      ]
    });
    this.alert.present();
  }

  deleteRoute(): void {
    let total: number = this.flightPlan.gates.length;
    for (let i: number = 0; i < total; i++) {
      this.deleteGate();
    }
  }

  saveRoute(): void {
    if (this.flightPlan.polylines.length > 0) {
      this.alert = this.alertCtrl.create(
        {
          title: 'Name',
          message: 'Enter route name',
          inputs: [{
            name: 'name',
            placeholder: 'Route name',
            type: 'string'
          }],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Ok',
              handler: (data: any) => {
                let legs: ILatLng[][] = [];
                for (let poly of this.flightPlan.polylines) {
                  let leg: ILatLng[] = [];
                  for (let pos of poly.getPath().getArray()) {
                    leg.push({ lat: pos.lat(), lng: pos.lng() });
                  }
                  legs.push(leg);
                }
                let route: IRoute = {
                  name: data.name || 'Untitled route',
                  date: new Date().getTime(),
                  legs: legs
                };
                this.database.saveRoute(route).then(() => {
                  this.evets.publish('save_route');
                });
                this.toast = this.toastCtrl.create(
                  {
                    message: 'Route saved!',
                    duration: 3000,
                    showCloseButton: true
                  }
                );
                this.toast.present();
              }
            }
          ]
        }
      );
      this.alert.present();
    } else {
      this.toast = this.toastCtrl.create(
        {
          message: 'Nothing to save!',
          duration: 3000,
          showCloseButton: true
        }
      );
      this.toast.present();
    }
  }

  async loadRoute(index: number): Promise<any> {
    this.deleteRoute();
    let route: IRoute = await this.database.getRoute(index);
    for (let i: number = 0; i < route.legs.length; i++) { // setup gates
      for (let j: number = 0; j < route.legs[i].length; j++) {
        if (j == 0) {
          this.addGate(route.legs[i][j]);
        }
        if (i == route.legs.length - 1 && j == route.legs[i].length - 1) {
          this.addGate(route.legs[i][j]);
        }
      }
    }
    for (let i: number = 0; i < route.legs.length; i++) { // setup polyline
      for (let j: number = 0; j < route.legs[i].length; j++) {
        this.flightPlan.polylines[i].getPath().setAt(
          j,
          new google.maps.LatLng(route.legs[i][j].lat, route.legs[i][j].lng));
      }
    }
  }

  ionViewDidLoad(): void {

    // tslint:disable-next-line:no-string-literal
    window['onGoogleMapsLoaded'] = () => {
      this.loadMap();
    };

    let node: any = document.createElement('script');
    node.src = CONST.GOOGLE_MAPS_URL;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);

    this.evets.subscribe('fly_route', (index: number) => {
      this.loadRoute(index);
    });

    this.evets.subscribe('settings_updated', () => {
      for (let g of this.flightPlan.gates) {
        g.eta = null;
        g.circle.setRadius(Settings.current.GATE_RADIUS_METERS);
      }
    });

    setInterval(() => {
      this.timeNow = Utils.getUTCTimeNow();
    }, 500, 0);

  }

  ionViewCanLeave(): boolean {
    return false;
  }

}
