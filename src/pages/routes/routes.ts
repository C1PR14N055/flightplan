import { Component } from '@angular/core';
import { AlertController, ToastController, Events, ItemSliding } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-routes',
  templateUrl: 'routes.html',
})
export class RoutesPage {

  routes: IRoute[];
  alert: any;
  toast: any;

  constructor(private alertCtrl: AlertController, private toastCtrl: ToastController,
    private database: DatabaseProvider, private events: Events) {
  }

  promptDeleteRoute(route: IRoute, index: number, slidingItem: ItemSliding): void {
    this.alert = this.alertCtrl.create(
      {
        title: 'Delete',
        message: `Are you sure you want to delete route '${route.name}'?`,
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              slidingItem.close();
            }
          },
          {
            text: 'Yes',
            handler: () => {
              slidingItem.close();
              this.deleteRoute(index);
            }
          }
        ]
      }
    );
    this.alert.present();
  }

  promptFlyRoute(route: IRoute, index: number, slidingItem: ItemSliding): void {
    this.alert = this.alertCtrl.create(
      {
        title: 'Fly it!',
        message: `Unsaved routes will be lost, are you sure you want to setup route '${route.name}'?`,
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              slidingItem.close();
            }
          },
          {
            text: 'Yes',
            handler: () => {
              slidingItem.close();
              this.flyRoute(index);
            }
          }
        ]
      }
    );
    this.alert.present();
  }

  async getRoutes(): Promise<any> {
    this.routes = await this.database.getRoutes();
  }

  async deleteRoute(index: number): Promise<any> {
    await this.database.deleteRoute(index);
    this.getRoutes();
  }

  flyRoute(index: number): void {
    this.events.publish('fly_route', index);
    this.toast = this.toastCtrl.create(
      {
        message: 'Route set up for flying!',
        duration: 3000,
        showCloseButton: true
      }
    );
    this.toast.present();
  }

  ionViewDidLoad(): void {
    this.getRoutes();
    this.events.subscribe('save_route', () => {
      this.getRoutes();
    });
  }

}
