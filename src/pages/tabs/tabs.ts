import { Component } from '@angular/core';

import { NavigationPage } from '../navigation/navigation';
import { RoutesPage } from '../routes/routes';
import { SettingsPage } from '../settings/settings';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root: any = NavigationPage;
  tab2Root: any = RoutesPage;
  tab3Root: any = SettingsPage;

  constructor() {

  }
}
