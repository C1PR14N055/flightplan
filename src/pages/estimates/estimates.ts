import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Utils } from '../../app/utils';
import { Settings } from '../../app/settings';

@Component({
  selector: 'page-estimates',
  templateUrl: 'estimates.html',
})
export class EstimatesPage {

  startGateTime: string = null;
  flightPlan: IFlightPlan = null;

  constructor(private viewCtrl: ViewController, private navParams: NavParams) {
    this.flightPlan = this.navParams.get('flightPlan');
    this.startGateTime = this.flightPlan.gates[0].eta
      ? new Date(this.flightPlan.gates[0].eta).toISOString()
      : new Date().toISOString();
    this.calculateEstimates();
  }

  calculateEstimates(): void {
    if (this.flightPlan.gates.length > 1) { // has more than 1 gate => has poly
      this.flightPlan.gates[0].eta = new Date(this.startGateTime).getTime();
      for (let g of this.flightPlan.gates) {
        if (g.nr > 0) {
          let coords: ILatLng[] = [];
          for (let i: number = 0; i < this.flightPlan.polylines[g.nr - 1].getPath().getLength(); i++) {
            let point: ILatLng = { lat: 0, lng: 0 };
            point.lat = this.flightPlan.polylines[g.nr - 1].getPath().getAt(i).lat();
            point.lng = this.flightPlan.polylines[g.nr - 1].getPath().getAt(i).lng();
            coords.push(point);
          }
          g.eta = this.flightPlan.gates[g.nr - 1].eta + Math.round(Utils.time(coords) * 1000) +
            (this.flightPlan.gates[g.nr - 1].proc ? Settings.current.PROCEDURE_TIME_SECONDS * 1000 : 0);
        }
      }
    }
  }

  toggleProcedure(event: any, index: number): void {
    this.flightPlan.gates[index].proc = event.value;
    this.calculateEstimates();
  }

  dismiss(): void {
    this.viewCtrl.dismiss(this.flightPlan);
  }

}
